package com.example.bookingticketproject.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.database_handler.DatabaseHandlerCustomers;
import com.example.bookingticketproject.entity.Account;
import com.example.bookingticketproject.entity.Customer;

import java.util.Calendar;

public class RegisterActivity extends AppCompatActivity {

    EditText txtFullName;
    EditText txtDob;
    RadioButton rBtnMale;
    RadioButton rBtnFemale;
    EditText txtPhoneNumber;
    EditText txtAddress;
    EditText txtEmail;
    EditText txtPassword;
    EditText txtPasswordConfirm;
    TextView txtErrorMsg;

    DatabaseHandlerAccounts handlerAccounts;
    DatabaseHandlerCustomers handlerCustomers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Get values on screen
        txtFullName = findViewById(R.id.txtFullName);
        txtDob = findViewById(R.id.txtDob);
        rBtnMale = findViewById(R.id.rBtnMale);
        rBtnFemale = findViewById(R.id.rBtnFemale);
        txtPhoneNumber = findViewById(R.id.textViewPhoneNumberDisplay);
        txtAddress = findViewById(R.id.textViewAddressDisplay);
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        txtPasswordConfirm = findViewById(R.id.txtPasswordConfirm);
        txtErrorMsg = findViewById(R.id.txtErrorMsg);

        txtDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        handlerAccounts = new DatabaseHandlerAccounts(this, null, null, 1);
        handlerCustomers = new DatabaseHandlerCustomers(this, null, null, 1);

    }

    public void btnRegisterOnClick(View view) {
        if (!isBlankInformation(txtFullName, txtDob, txtPhoneNumber, txtAddress, txtEmail, txtPassword, txtPasswordConfirm)) {
            String fullName = txtFullName.getText().toString();

            String dob = txtDob.getText().toString();
//            try {
//                dob = new SimpleDateFormat("dd/MM/yyyy").parse(txtDob.getText().toString());
//            } catch (ParseException e) {
//                e.printStackTrace();
//                System.err.println("Error parsing date!");
//            }
            boolean gender = rBtnMale.isChecked() ? true : false;
            String phoneNumber = txtPhoneNumber.getText().toString();
            String address = txtAddress.getText().toString();
            String email = txtEmail.getText().toString();
            String password = txtPassword.getText().toString();
            String passwordConfirm = txtPasswordConfirm.getText().toString();

            if (isExistedEmail(email)) {
                txtErrorMsg.setText("*Email already existed!*");
                return;
            } else if (!password.equals(passwordConfirm)) {
                txtErrorMsg.setText("*Password and confirmation does not match!*");
                return;
            } else {
                // Add account
                Account account = new Account(email, password, "user", true);
                handlerAccounts.insertAccount(account);
                // Add customer
                Customer customer = new Customer(fullName, address, dob, gender, phoneNumber, handlerAccounts.findAccountByEmail(email).getAccount_ID());
                handlerCustomers.insertCustomers(customer);

                txtErrorMsg.setText("");
                Toast.makeText(this, "Successfully registered!", Toast.LENGTH_SHORT).show();

                // Back to home activity
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
            }

        } else {
            txtErrorMsg.setText("*Must fill in all information to register!*");
        }
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                txtDob.setText(dayOfMonth + "/" + ((month + 1) < 10 ? "0" + (month + 1) : (month + 1)) + "/" + year);
            }
        }, year, month, day);

        dialog.show();
    }

    private boolean isBlankInformation(EditText... editTexts) {
        for (EditText editText: editTexts) {
            if (editText == null || ("").equals(editText.getText().toString())) {
                return true;
            }
        }
        return false;
    }

    private boolean isExistedEmail(String eMail) {
        if (handlerAccounts.findAccountByEmail(eMail) != null) {
            return true;
        } else {
            return false;
        }
    }

}