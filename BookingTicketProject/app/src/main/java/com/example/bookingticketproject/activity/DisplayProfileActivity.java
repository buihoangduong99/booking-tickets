package com.example.bookingticketproject.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.database_handler.DatabaseHandlerCustomers;
import com.example.bookingticketproject.entity.Account;
import com.example.bookingticketproject.entity.Customer;
import com.example.bookingticketproject.session.AccountSession;

public class DisplayProfileActivity extends AppCompatActivity {

    TextView textViewFullNameDisplay;
    TextView textViewEmailDisplay;
    TextView textViewDobDisplay;
    TextView textViewGenderDisplay;
    TextView textViewPhoneNumberDisplay;
    TextView textViewAddressDisplay;

    AccountSession session;

    DatabaseHandlerAccounts handlerAccounts;
    DatabaseHandlerCustomers handlerCustomers;

    Account account;
    Customer customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_profile);

        session = new AccountSession(this);

        // Get the text views of screen
        textViewFullNameDisplay = findViewById(R.id.txtTitle);
        textViewEmailDisplay = findViewById(R.id.textViewEmailDisplay);
        textViewDobDisplay = findViewById(R.id.textViewDobDisplay);
        textViewGenderDisplay = findViewById(R.id.textViewGenderDisplay);
        textViewPhoneNumberDisplay = findViewById(R.id.textViewPhoneNumberDisplay);
        textViewAddressDisplay = findViewById(R.id.textViewAddressDisplay);

        // Get the object data for account and customer
        handlerAccounts = new DatabaseHandlerAccounts(this, null, null, 1);
        handlerCustomers = new DatabaseHandlerCustomers(this, null, null, 1);
        account = handlerAccounts.findAccountByID(session.getAccountId());
        customer = handlerCustomers.findCustomerByAccountID(account.getAccount_ID());

        // Set the text views to corresponding data
        textViewFullNameDisplay.setText(customer.getCustomer_fullname());
        textViewEmailDisplay.setText(account.getAccount_email());
        textViewDobDisplay.setText(customer.getCustomer_dob());
        textViewGenderDisplay.setText(customer.isCustomer_gender() ? "Male" : "Female");
        textViewPhoneNumberDisplay.setText(customer.getCustomer_phonenumber());
        textViewAddressDisplay.setText(customer.getCustomer_address());

    }

    public void btnChangeInfoOnClick(View view) {
        EditProfileDialog editProfileDialog = new EditProfileDialog(DisplayProfileActivity.this, customer);

        // Catch dialog dismiss event
        editProfileDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });

        editProfileDialog.show();

        // Resize dialog
        Display display = ((WindowManager) getSystemService(this.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        Log.v("width", width + "");
        editProfileDialog.getWindow().setLayout(width, (4 * height) / 6);
    }

    public void btnChangePasswordOnClick(View view) {
        ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog(this, account);

        changePasswordDialog.show();

        // Resize dialog
        Display display = ((WindowManager) getSystemService(this.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        Log.v("width", width + "");

        changePasswordDialog.getWindow().setLayout(width, (5 * height) / 8);
    }

    public void btnLogoutOnClick(View view) {
        // Update session
        session.setAccountId(0);

        // Show Toast
        Toast.makeText(this, "Logged out", Toast.LENGTH_SHORT).show();

        // Back to home page
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    public void btnHistoryOnClick(View view) {
        Intent intent = new Intent(this, HistoryBookingActivity.class);
        startActivity(intent);
    }

}