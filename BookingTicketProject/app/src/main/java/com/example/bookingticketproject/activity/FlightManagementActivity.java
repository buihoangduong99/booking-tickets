package com.example.bookingticketproject.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.bookingticketproject.adapter.ListFlightManageAdapter;
import com.example.bookingticketproject.R;
import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.database_handler.DatabaseHandlerFlights;
import com.example.bookingticketproject.entity.Account;
import com.example.bookingticketproject.entity.Flight;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Locale;

public class FlightManagementActivity extends AppCompatActivity {


    ListView listView;
    Spinner spinnerfrom;
    Spinner spinnerto;
    ListFlightManageAdapter listFlightManageAdapter;
    DatabaseHandlerFlights handlerFlights;
    List<Flight> listFlighManagement;
    final Calendar myCalendar = Calendar.getInstance();
    EditText txtDepartureDay;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_management);

        handlerFlights = new DatabaseHandlerFlights(this, null, null, 1);

        listView = findViewById(R.id.listViewFlights);
        spinnerfrom = findViewById(R.id.spinnerFrom);
        spinnerto = findViewById(R.id.spinnerTo);

        txtDepartureDay = findViewById(R.id.txtDepartureDay);
        txtDepartureDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(FlightManagementActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    public void btnSearchOnClick(View view) {
        String from = spinnerfrom.getSelectedItem().toString();
        String to = spinnerto.getSelectedItem().toString();
        String departure_day = txtDepartureDay.getText().toString();
        listFlighManagement = handlerFlights.searchListFlight(from,to,departure_day);
        if (isNullOrEmptyList(listFlighManagement)) {
            listFlighManagement = new ArrayList<>();
        }
        listView.setAdapter(new ListFlightManageAdapter(this, listFlighManagement));

    }


    private boolean isBlankFromTo(Spinner spinnerFrom, Spinner spinnerTo) {
        if (spinnerFrom == null && spinnerTo == null
                || ("").equals(spinnerFrom.getSelectedItem().toString())
                && ("").equals(spinnerTo.getSelectedItem().toString())) {
            return true;
        }
        return false;
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        txtDepartureDay.setText(sdf.format(myCalendar.getTime()));
    }

    public void btnAddFlight(View view) {
        Intent intent = new Intent(this, SaveFlightActivity.class);
        startActivity(intent);
//        String from = spinnerfrom.getSelectedItem().toString();
//        String to = spinnerto.getSelectedItem().toString();
//        String departure_day = txtDepartureDay.getText().toString();
//        listFlighManagement = handlerFlights.searchListFlight(from, to, departure_day);
//        listView.setAdapter(new ListFlightManageAdapter(this, listFlighManagement));
//        listView.setAdapter(new ListFlightManageAdapter(this,Collections.<Flight>emptyList()));
    }

    public boolean isNullOrEmptyList(List<Flight> flights) {
        if (flights == null || flights.isEmpty()) {
            Toast.makeText(this, "No flight found!", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }
    }

}