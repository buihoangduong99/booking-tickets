package com.example.bookingticketproject.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.database_handler.DatabaseHandlerFlights;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.view.View.OnClickListener;

public class SearchFlightActivity extends AppCompatActivity {

    NumberPicker numberPassenger;

    Spinner spinnerfrom;
    Spinner spinnerto;
    Switch switch1;
    Spinner spinnerseatsclass;

    final Calendar myCalendar = Calendar.getInstance();
    EditText departureDay;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_flight);

        // Get Views
        spinnerfrom = findViewById(R.id.spinnerfrom);
        spinnerto = findViewById(R.id.spinnerto);
        departureDay = findViewById(R.id.departureDay);

        numberPassenger = findViewById(R.id.numberpassenger);
        numberPassenger.setMaxValue(11);
        numberPassenger.setMinValue(1);
        numberPassenger.setValue(2);

        switch1 = findViewById(R.id.switch1);
        spinnerseatsclass = findViewById(R.id.spinnerseatsclass);

        departureDay = findViewById(R.id.departureDay);
        departureDay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SearchFlightActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        departureDay.setText(sdf.format(myCalendar.getTime()));
    }

    public void btnSearchOnClick(View view) {
        Intent intent = new Intent(this, ListFlightActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constant.FLIGHT_COLUMN_ADDRESS_FROM, spinnerfrom.getSelectedItem().toString());
        bundle.putString(Constant.FLIGHT_COLUMN_ADDRESS_TO, spinnerto.getSelectedItem().toString());
        bundle.putString(Constant.FLIGHT_COLUMN_DEPARTURE_DAY, departureDay.getText().toString());
        bundle.putBoolean(Constant.BOOKING_FLIGHT_COLUMN_ROUND_TRIP, switch1.isChecked());
        bundle.putInt(Constant.BOOKING_FLIGHT_COLUMN_PASSENGER, (numberPassenger.getValue() - 1));
        bundle.putString(Constant.BOOKING_FLIGHT_COLUMN_SEAT_CLASS, spinnerseatsclass.getSelectedItem().toString());
        intent.putExtras(bundle);
        startActivity(intent);
    }

}