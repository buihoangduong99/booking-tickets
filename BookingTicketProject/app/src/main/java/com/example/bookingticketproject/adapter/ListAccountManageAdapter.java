package com.example.bookingticketproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingticketproject.activity.AccountManagementActivity;
import com.example.bookingticketproject.R;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.database_handler.DatabaseHandlerCustomers;
import com.example.bookingticketproject.entity.Account;
import com.example.bookingticketproject.entity.Customer;

import java.util.List;

public class ListAccountManageAdapter extends BaseAdapter {

    List<Account> accounts;
    Context context;

    DatabaseHandlerAccounts handlerAccounts;
    DatabaseHandlerCustomers handlerCustomers;

    private static LayoutInflater inflater = null;

    public ListAccountManageAdapter (AccountManagementActivity accountManagementActivity, List<Account> accounts) {
        this.accounts = accounts;
        this.context = accountManagementActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        handlerAccounts = new DatabaseHandlerAccounts(this.context, null, null, 1);
        handlerCustomers = new DatabaseHandlerCustomers(this.context, null, null, 1);
    }

    @Override
    public int getCount() {
        return accounts.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.adapter_list_account_manage, null);

        // Get views
        TextView textViewFullName = rowView.findViewById(R.id.textViewFullName);
        TextView textViewEmail = rowView.findViewById(R.id.textViewEmail);
        Switch switchLock = rowView.findViewById(R.id.switchLock);

        // Get the customer by account id
        final Account a = this.accounts.get(position);
        Customer customer = this.handlerCustomers.findCustomerByAccountID(this.accounts.get(position).getAccount_ID());

        // Set values
        textViewFullName.setText(customer.getCustomer_fullname());
        textViewEmail.setText(this.accounts.get(position).getAccount_email());
        if (this.accounts.get(position).isStatus()) {
            switchLock.setChecked(true);
        } else {
            switchLock.setChecked(false);
        }

        switchLock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    handlerAccounts.updateAccountStatusById(a.getAccount_ID(), true);
                    Toast.makeText(context, "Unlocked user: " + a.getAccount_email(), Toast.LENGTH_SHORT).show();
                } else {
                    handlerAccounts.updateAccountStatusById(a.getAccount_ID(), false);
                    Toast.makeText(context, "Locked user: " + a.getAccount_email(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rowView;
    }
}
