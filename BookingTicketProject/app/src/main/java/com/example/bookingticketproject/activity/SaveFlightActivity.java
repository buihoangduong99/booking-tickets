package com.example.bookingticketproject.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.database_handler.DatabaseHandlerFlights;
import com.example.bookingticketproject.entity.Flight;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SaveFlightActivity extends AppCompatActivity {

    Spinner spinnerBranchFlight;
    Spinner spinnerFrom;
    Spinner spinnerTo;
    Spinner spinnerTimeFrom;
    Spinner spinnerTimeTo;
    EditText txtEconomy;
    EditText txtBusiness;
    EditText txtFirstClass;
    EditText departureDay;

    //database
    DatabaseHandlerFlights handlerfli;

    //date
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };
    String flightID = null;
    Flight flight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_flight_activity);
        getView();
        departureDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SaveFlightActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        makeContextDatabse();

        try {
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            flightID = bundle.getString(Constant.FLIGHT_COLUMN_FLIGHT_ID);
            flight = handlerfli.findFlightByID(flightID);
            setTextUpdate(flight);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

    }

    public void getView() {
        spinnerBranchFlight = findViewById(R.id.spinnerBranchFlight);
        spinnerFrom = findViewById(R.id.spinnerFrom);
        spinnerTo = findViewById(R.id.spinnerTo);
        spinnerTimeFrom = findViewById(R.id.spinnerTimeFrom);
        spinnerTimeTo = findViewById(R.id.spinnerTimeTo);
        txtEconomy = findViewById(R.id.txtEconomy);
        txtBusiness = findViewById(R.id.txtBusiness);
        txtFirstClass = findViewById(R.id.txtFirstClass);
        departureDay = findViewById(R.id.departureDayManage);
    }

    public void makeContextDatabse() {
        handlerfli = new DatabaseHandlerFlights(this, Constant.DATABASE_NAME, null, 1);
    }

    public void btnAddFlight(View view) {

        Intent intent;

        makeContextDatabse();

        if (flightID == null) {
            //get String Flight
            String strBranchFlight = spinnerBranchFlight.getSelectedItem().toString();
            String strAddress_to = spinnerTo.getSelectedItem().toString();
            String strAddress_from = spinnerFrom.getSelectedItem().toString();
            String strTimeFrom = spinnerTimeFrom.getSelectedItem().toString();
            String strTimeTo = spinnerTimeTo.getSelectedItem().toString();
            String strdepartureDay = departureDay.getText().toString();
            int economy_total = 0;
            int business_total = 0;
            int firstclass_total = 0;
            try {
                economy_total = Integer.parseInt(txtEconomy.getText().toString());
                business_total = Integer.parseInt(txtBusiness.getText().toString());
                firstclass_total = Integer.parseInt(txtFirstClass.getText().toString());
            } catch (Exception e) {
                Toast.makeText(this, "Must fill all data", Toast.LENGTH_LONG).show();
                intent = new Intent(this, SaveFlightActivity.class);
                startActivity(intent);
                return;
            }

            //add fail
            if (strAddress_from.equalsIgnoreCase(strAddress_to)) {
                Toast.makeText(this, "Submit Fail", Toast.LENGTH_LONG).show();
                intent = new Intent(this, SaveFlightActivity.class);
            }
            //add success
            else {
                List<Flight> flights = handlerfli.getAllFlight();
                //create flight
                flight = new Flight();
                flight.setFlight_ID("VJ00" + flights.size() + 1);
                flight.setBrand_flight(strBranchFlight);
                flight.setAddress_from(strAddress_from);
                flight.setAddress_to(strAddress_to);
                flight.setTime_flight_from(strTimeFrom);
                flight.setGetTime_flight_to(strTimeTo);
                flight.setEconomy_total(economy_total);
                flight.setBusiness_total(business_total);
                flight.setFirstclass_total(firstclass_total);
                //
                flight.setEconomy_price(1);
                flight.setBusiness_price(2);
                flight.setFirstclass_price(3);
                //
                flight.setDeparture_day(strdepartureDay);
                //add Flight to db
                handlerfli.insertFlight(flight);
                Toast.makeText(this, "Successfully", Toast.LENGTH_LONG).show();
                intent = new Intent(this, FlightManagementActivity.class);
            }
            startActivity(intent);
        }
        //update
        else {
            flight = handlerfli.findFlightByID(flightID);
            flight.setGetTime_flight_from(spinnerTimeFrom.getSelectedItem().toString());
            flight.setGetTime_flight_to(spinnerTimeTo.getSelectedItem().toString());
            flight.setDeparture_day(departureDay.getText().toString());
            handlerfli.updateFlight(flight);
            Toast.makeText(this, "Update Successfully", Toast.LENGTH_LONG).show();
            intent = new Intent(this, FlightManagementActivity.class);
            startActivity(intent);
        }
    }

    public void setTextUpdate(Flight flight) {
        spinnerBranchFlight.setSelection(setSpinnerBrand(flight.getBrand_flight()));
        spinnerFrom.setSelection(setSpinnerAddress(flight.getAddress_from()));
        spinnerTo.setSelection(setSpinnerAddress(flight.getAddress_to()));
        spinnerTimeFrom.setSelection(setSpinnerTime(flight.getGetTime_flight_from()));
        spinnerTimeTo.setSelection(setSpinnerTime(flight.getGetTime_flight_to()));
        txtEconomy.setText(flight.getEconomy_total() + "");
        txtBusiness.setText(flight.getBusiness_total() + "");
        txtFirstClass.setText(flight.getFirstclass_total() + "");
        departureDay.setText(flight.getDeparture_day() + "");
    }

    public int setSpinnerAddress(String city) {
        String[] cites = getResources().getStringArray(R.array.cities);
        for (int i = 0; i < cites.length; i++) {
            if (cites[i].equals(city)) {
                return i;
            }
        }
        return 0;
    }

    public int setSpinnerBrand(String brand) {
        String[] cites = getResources().getStringArray(R.array.airlines);
        for (int i = 0; i < cites.length; i++) {
            if (cites[i].equals(brand)) {
                return i;
            }
        }
        return 0;
    }

    public int setSpinnerTime(String time) {
        String[] cites = getResources().getStringArray(R.array.time);
        for (int i = 0; i < cites.length; i++) {
            if (cites[i].equals(time)) {
                return i;
            }
        }
        return 0;
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        departureDay.setText(sdf.format(myCalendar.getTime()));
    }
}