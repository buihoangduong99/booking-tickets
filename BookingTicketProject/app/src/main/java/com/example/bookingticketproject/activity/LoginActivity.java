package com.example.bookingticketproject.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.bookingticketproject.R;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.entity.Account;
import com.example.bookingticketproject.session.AccountSession;

public class LoginActivity extends AppCompatActivity {

    EditText txtEmail;
    EditText txtPassword;
    TextView txtErrorMsg;

    DatabaseHandlerAccounts handlerAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        txtErrorMsg = findViewById(R.id.txtErrorMsg);

        handlerAccounts = new DatabaseHandlerAccounts(this, null, null, 1);
    }

    public void btnLoginOnclick(View view) {
        if (!isBlankInformation(txtEmail, txtPassword)) {
            String eMail = txtEmail.getText().toString();
            String password = txtPassword.getText().toString();

            Account a = handlerAccounts.findAccountByEmailPassword(eMail, password);
            if (a != null) {
                // Check for locked account
                if (!a.isStatus()) {
                    // Show Toast
                    Toast.makeText(this, "Unable to login!", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Update session
                AccountSession session = new AccountSession(this);
                session.setAccountId(a.getAccount_ID());

                // Show Toast
                Toast.makeText(this, "Logged in successfully!", Toast.LENGTH_SHORT).show();

                txtErrorMsg.setText("");

                // Back to home page
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                return;
            }
            txtErrorMsg.setText("Failed! Please check your information.");

        } else {
            txtErrorMsg.setText("Must fill in all information to login!");
        }
    }

    public void btnForgotPasswordOnclick(View view) {
        ForgotPasswordDialog forgotPasswordDialog = new ForgotPasswordDialog(this);
        forgotPasswordDialog.show();

        // Resize dialog
        Display display = ((WindowManager) getSystemService(this.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        Log.v("width", width + "");

        forgotPasswordDialog.getWindow().setLayout(width, (8 * height) / 11);
    }

    public void btnRegisterAccountOnClick(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private boolean isBlankInformation(EditText... editTexts) {
        for (EditText editText : editTexts) {
            if (editText == null || ("").equals(editText.getText().toString())) {
                return true;
            }
        }
        return false;
    }

}