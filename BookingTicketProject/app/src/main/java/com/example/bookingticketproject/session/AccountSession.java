package com.example.bookingticketproject.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AccountSession {
    private SharedPreferences prefs;

    public AccountSession(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public int getAccountId() {
        int accountId = prefs.getInt("accountId", 0);
        return accountId;
    }

    public void setAccountId(int accountId) {
        prefs.edit().putInt("accountId", accountId).commit();
    }

}
