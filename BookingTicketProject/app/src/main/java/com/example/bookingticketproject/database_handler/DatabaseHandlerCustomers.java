package com.example.bookingticketproject.database_handler;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.entity.Customer;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandlerCustomers extends SQLiteOpenHelper {


    ContentResolver resolver;

    public DatabaseHandlerCustomers(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, Constant.DATABASE_NAME, factory, Constant.VERSION);
        resolver = context.getContentResolver();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constant.CREATE_TABLE_ACCOUNT);
        db.execSQL(Constant.CREATE_TABLE_BOOKING_FLIGHT);
        db.execSQL(Constant.CREATE_TABLE_CUSTOMER);
        db.execSQL(Constant.CREATE_TABLE_FLIGHT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + Constant.CUSTOMER_TABLE_NAME);
        onCreate(db);
    }

    public Long insertCustomers(Customer customer) {
        ContentValues values = new ContentValues();
//        values.put(Constant.CUSTOMER_COLUMN_CUSTOMER_ID, customer.getCustomer_ID());
        values.put(Constant.CUSTOMER_COLUMN_CUSTOMER_FULL_NAME, customer.getCustomer_fullname());
        values.put(Constant.CUSTOMER_COLUMN_CUSTOMER_ADDRESS, customer.getCustomer_address());
        values.put(Constant.CUSTOMER_COLUMN_CUSTOMER_DOB, customer.getCustomer_dob());
        values.put(Constant.CUSTOMER_COLUMN_CUSTOMER_GENDER, customer.isCustomer_gender());
        values.put(Constant.CUSTOMER_COLUMN_CUSTOMER_PHONE_NUMBER, customer.getCustomer_phonenumber());
        values.put(Constant.CUSTOMER_COLUMN_ACCOUNT_ID, customer.getAccount_ID());


        SQLiteDatabase db = getWritableDatabase();
        Long result = db.insert(Constant.CUSTOMER_TABLE_NAME, null, values);

        db.close();

        return result;
    }

    public List<Customer> getAllCustomer() {
        List<Customer> customers = new ArrayList<>();
        String query = " SELECT * FROM " + Constant.CUSTOMER_TABLE_NAME;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Customer cus = new Customer();
                cus.setCustomer_ID(cursor.getInt(0));
                cus.setCustomer_fullname(cursor.getString(1));
                cus.setCustomer_address(cursor.getString(2));
                cus.setCustomer_dob(cursor.getString(3));
                cus.setCustomer_gender("0".equals(cursor.getString(4)) ? false : true);
                cus.setCustomer_phonenumber(cursor.getString(5));
                cus.setAccount_ID(cursor.getInt(6));
                customers.add(cus);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return customers;
        }
        cursor.close();
        db.close();
        return null;
    }

    public Customer findCustomerByID(int id) {
        String query = " SELECT * FROM " + Constant.CUSTOMER_TABLE_NAME + " WHERE " + Constant.CUSTOMER_COLUMN_CUSTOMER_ID + " ='" + id + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            Customer cus = new Customer();
            cus.setCustomer_ID(cursor.getInt(0));
            cus.setCustomer_fullname(cursor.getString(1));
            cus.setCustomer_address(cursor.getString(2));
            cus.setCustomer_dob(cursor.getString(3));
            cus.setCustomer_gender("0".equals(cursor.getString(4)) ? false : true);
            cus.setCustomer_phonenumber(cursor.getString(5));
            cus.setAccount_ID(cursor.getInt(6));
            cursor.close();
            db.close();
            return cus;
        }
        cursor.close();
        db.close();
        return null;
    }

    public Customer findCustomerByAccountID(int id) {
        String query = " SELECT * FROM " + Constant.CUSTOMER_TABLE_NAME + " WHERE " + Constant.CUSTOMER_COLUMN_ACCOUNT_ID + " ='" + id + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            Customer cus = new Customer();
            cus.setCustomer_ID(cursor.getInt(0));
            cus.setCustomer_fullname(cursor.getString(1));
            cus.setCustomer_address(cursor.getString(2));
            cus.setCustomer_dob(cursor.getString(3));
            cus.setCustomer_gender("0".equals(cursor.getString(4)) ? false : true);
            cus.setCustomer_phonenumber(cursor.getString(5));
            cus.setAccount_ID(cursor.getInt(6));
            cursor.close();
            db.close();
            return cus;
        }
        cursor.close();
        db.close();
        return null;
    }

    public Customer updateCustomerByCustomerId(Customer customer) {
        SQLiteDatabase db = getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.CUSTOMER_COLUMN_CUSTOMER_FULL_NAME, customer.getCustomer_fullname());
        contentValues.put(Constant.CUSTOMER_COLUMN_CUSTOMER_ADDRESS, customer.getCustomer_address());
        contentValues.put(Constant.CUSTOMER_COLUMN_CUSTOMER_DOB, customer.getCustomer_dob());
        contentValues.put(Constant.CUSTOMER_COLUMN_CUSTOMER_GENDER, customer.isCustomer_gender());
        contentValues.put(Constant.CUSTOMER_COLUMN_CUSTOMER_PHONE_NUMBER, customer.getCustomer_phonenumber());

        int result = db.update(Constant.CUSTOMER_TABLE_NAME, contentValues, Constant.CUSTOMER_COLUMN_CUSTOMER_ID + " = ?", new String[]{customer.getCustomer_ID() + ""});
        if (result == -1) {
            return null;
        } else {
            return customer;
        }
    }

}
