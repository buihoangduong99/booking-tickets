package com.example.bookingticketproject.database_handler;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.entity.Customer;
import com.example.bookingticketproject.entity.Flight;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHandlerFlights extends SQLiteOpenHelper {

    ContentResolver resolver;

    public DatabaseHandlerFlights(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, Constant.DATABASE_NAME, factory, Constant.VERSION);
        resolver = context.getContentResolver();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constant.CREATE_TABLE_ACCOUNT);
        db.execSQL(Constant.CREATE_TABLE_BOOKING_FLIGHT);
        db.execSQL(Constant.CREATE_TABLE_CUSTOMER);
        db.execSQL(Constant.CREATE_TABLE_FLIGHT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + Constant.DATABASE_NAME);
        onCreate(db);
    }

    public Long insertFlight(Flight flight) {
        ContentValues values = new ContentValues();
        values.put(Constant.FLIGHT_COLUMN_FLIGHT_ID, flight.getFlight_ID());
        values.put(Constant.FLIGHT_COLUMN_BRAND_FLIGHT, flight.getBrand_flight());
        values.put(Constant.FLIGHT_COLUMN_ADDRESS_FROM, flight.getAddress_from());
        values.put(Constant.FLIGHT_COLUMN_ADDRESS_TO, flight.getAddress_to());
        values.put(Constant.FLIGHT_COLUMN_DEPARTURE_DAY, flight.getDeparture_day());
        values.put(Constant.FLIGHT_COLUMN_TIME_FLIGHT_FROM, flight.getTime_flight_from());
        values.put(Constant.FLIGHT_COLUMN_TIME_FLIGHT_TO, flight.getGetTime_flight_to());
        values.put(Constant.FLIGHT_COLUMN_ECONOMY_TOTAL, flight.getEconomy_total());
        values.put(Constant.FLIGHT_COLUMN_ECONOMY_PRICE, flight.getEconomy_price());
        values.put(Constant.FLIGHT_COLUMN_BUSINESS_TOTAL, flight.getBusiness_total());
        values.put(Constant.FLIGHT_COLUMN_BUSINESS_PRICE, flight.getBusiness_price());
        values.put(Constant.FLIGHT_COLUMN_FIRST_CLASS_TOTAL, flight.getFirstclass_total());
        values.put(Constant.FLIGHT_COLUMN_FIRST_CLASS_PRICE, flight.getFirstclass_price());
        values.put(Constant.FLIGHT_COLUMN_COUPON, flight.getCoupon());
        values.put(Constant.FLIGHT_COLUMN_IMAGE, flight.getImage());

        SQLiteDatabase db = getWritableDatabase();
        Long result = db.insert(Constant.FLIGHT_TABLE_NAME, null, values);

        db.close();

        return result;
    }

    public List<Flight> getAllFlight() {
        List<Flight> flights = new ArrayList<>();
        String query = " SELECT * FROM " + Constant.FLIGHT_TABLE_NAME;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Flight flight = new Flight();
                flight.setFlight_ID(cursor.getString(0));
                flight.setBrand_flight(cursor.getString(1));
                flight.setAddress_from(cursor.getString(2));
                flight.setAddress_to(cursor.getString(3));
                flight.setDeparture_day(cursor.getString(4));
                flight.setGetTime_flight_from(cursor.getString(5));
                flight.setGetTime_flight_to(cursor.getString(6));
                flight.setEconomy_total(cursor.getInt(7));
                flight.setEconomy_price(cursor.getFloat(8));
                flight.setBusiness_total(cursor.getInt(9));
                flight.setBusiness_price(cursor.getFloat(10));
                flight.setFirstclass_total(cursor.getInt(11));
                flight.setFirstclass_price(cursor.getFloat(12));
                flight.setCoupon(cursor.getFloat(13));
                flight.setImage(cursor.getString(14));
                flights.add(flight);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return flights;
        }
        cursor.close();
        db.close();
        return null;
    }

    public List<Flight> searchListFlight(String from, String to, String depart_day) {
        List<Flight> flights = new ArrayList<>();
        String query = " SELECT * FROM " + Constant.FLIGHT_TABLE_NAME
                + " WHERE " + Constant.FLIGHT_COLUMN_ADDRESS_FROM
                + " LIKE '%" + from + "%'"
                + " AND " + Constant.FLIGHT_COLUMN_ADDRESS_TO
                + " LIKE '%" + to + "%' AND "
                + Constant.FLIGHT_COLUMN_DEPARTURE_DAY + " LIKE '%" + depart_day + "%'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Flight flight = new Flight();
                flight.setFlight_ID(cursor.getString(0));
                flight.setBrand_flight(cursor.getString(1));
                flight.setAddress_from(cursor.getString(2));
                flight.setAddress_to(cursor.getString(3));
                flight.setDeparture_day(cursor.getString(4));
                flight.setGetTime_flight_from(cursor.getString(5));
                flight.setGetTime_flight_to(cursor.getString(6));
                flight.setEconomy_total(cursor.getInt(7));
                flight.setEconomy_price(cursor.getFloat(8));
                flight.setBusiness_total(cursor.getInt(9));
                flight.setBusiness_price(cursor.getFloat(10));
                flight.setFirstclass_total(cursor.getInt(11));
                flight.setFirstclass_price(cursor.getFloat(12));
                flight.setCoupon(cursor.getFloat(13));
                flight.setImage(cursor.getString(14));
                flights.add(flight);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return flights;
        }
        cursor.close();
        db.close();
        return null;
    }

    public List<Flight> searchListFlightWithBookInfo(String from, String to, String depart_day, String seat_type, int total_seats) {
        String SEAT_TYPE_COLUMN = "";

        if ("Economy".equals(seat_type)) {
            SEAT_TYPE_COLUMN = Constant.FLIGHT_COLUMN_ECONOMY_TOTAL;
        } else if ("Business".equals(seat_type)) {
            SEAT_TYPE_COLUMN = Constant.FLIGHT_COLUMN_BUSINESS_TOTAL;
        } else if ("First Class".equals(seat_type)) {
            SEAT_TYPE_COLUMN = Constant.FLIGHT_COLUMN_FIRST_CLASS_TOTAL;
        }

        List<Flight> flights = new ArrayList<>();
        String query = " SELECT * FROM " + Constant.FLIGHT_TABLE_NAME
                + " WHERE " + Constant.FLIGHT_COLUMN_ADDRESS_FROM
                + " LIKE '%" + from + "%'"
                + " AND " + Constant.FLIGHT_COLUMN_ADDRESS_TO
                + " LIKE '%" + to + "%' AND "
                + Constant.FLIGHT_COLUMN_DEPARTURE_DAY + " LIKE '%" + depart_day + "%'"
                + " AND " + SEAT_TYPE_COLUMN + " >= " + total_seats;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Flight flight = new Flight();
                flight.setFlight_ID(cursor.getString(0));
                flight.setBrand_flight(cursor.getString(1));
                flight.setAddress_from(cursor.getString(2));
                flight.setAddress_to(cursor.getString(3));
                flight.setDeparture_day(cursor.getString(4));
                flight.setGetTime_flight_from(cursor.getString(5));
                flight.setGetTime_flight_to(cursor.getString(6));
                flight.setEconomy_total(cursor.getInt(7));
                flight.setEconomy_price(cursor.getFloat(8));
                flight.setBusiness_total(cursor.getInt(9));
                flight.setBusiness_price(cursor.getFloat(10));
                flight.setFirstclass_total(cursor.getInt(11));
                flight.setFirstclass_price(cursor.getFloat(12));
                flight.setCoupon(cursor.getFloat(13));
                flight.setImage(cursor.getString(14));
                flights.add(flight);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return flights;
        }
        cursor.close();
        db.close();
        return null;
    }

    public Flight findFlightByID(String id) {
        String query = " SELECT * FROM " + Constant.FLIGHT_TABLE_NAME + " WHERE " + Constant.FLIGHT_COLUMN_FLIGHT_ID + " ='" + id + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            Flight flight = new Flight();
            flight.setFlight_ID(cursor.getString(0));
            flight.setBrand_flight(cursor.getString(1));
            flight.setAddress_from(cursor.getString(2));
            flight.setAddress_to(cursor.getString(3));
            flight.setDeparture_day(cursor.getString(4));
            flight.setGetTime_flight_from(cursor.getString(5));
            flight.setGetTime_flight_to(cursor.getString(6));
            flight.setEconomy_total(cursor.getInt(7));
            flight.setEconomy_price(cursor.getFloat(8));
            flight.setBusiness_total(cursor.getInt(9));
            flight.setBusiness_price(cursor.getFloat(10));
            flight.setFirstclass_total(cursor.getInt(11));
            flight.setFirstclass_price(cursor.getFloat(12));
            flight.setCoupon(cursor.getFloat(13));
            flight.setImage(cursor.getString(14));
            cursor.close();
            db.close();
            return flight;
        }
        cursor.close();
        db.close();
        return null;
    }

    public int deleteFlight(String id) {
        Flight flight = null;
        flight = findFlightByID(id);

        if (flight == null)
            return 0;

        SQLiteDatabase db = getWritableDatabase();
        int result = db.delete(Constant.DATABASE_NAME, Constant.FLIGHT_COLUMN_FLIGHT_ID + " =?", new String[]{flight.getFlight_ID()});
        db.close();
        return result;
    }

    public int updateFlight(Flight flight) {
        ContentValues values = new ContentValues();
        values.put(Constant.FLIGHT_COLUMN_FLIGHT_ID, flight.getFlight_ID());
        values.put(Constant.FLIGHT_COLUMN_BRAND_FLIGHT, flight.getBrand_flight());
        values.put(Constant.FLIGHT_COLUMN_ADDRESS_FROM, flight.getAddress_from());
        values.put(Constant.FLIGHT_COLUMN_ADDRESS_TO, flight.getAddress_to());
        values.put(Constant.FLIGHT_COLUMN_DEPARTURE_DAY, flight.getDeparture_day());
        values.put(Constant.FLIGHT_COLUMN_TIME_FLIGHT_FROM, flight.getTime_flight_from());
        values.put(Constant.FLIGHT_COLUMN_TIME_FLIGHT_TO, flight.getGetTime_flight_to());
        values.put(Constant.FLIGHT_COLUMN_ECONOMY_TOTAL, flight.getEconomy_total());
        values.put(Constant.FLIGHT_COLUMN_ECONOMY_PRICE, flight.getEconomy_price());
        values.put(Constant.FLIGHT_COLUMN_BUSINESS_TOTAL, flight.getBusiness_total());
        values.put(Constant.FLIGHT_COLUMN_BUSINESS_PRICE, flight.getBusiness_price());
        values.put(Constant.FLIGHT_COLUMN_FIRST_CLASS_TOTAL, flight.getFirstclass_total());
        values.put(Constant.FLIGHT_COLUMN_FIRST_CLASS_PRICE, flight.getFirstclass_price());
        values.put(Constant.FLIGHT_COLUMN_COUPON, flight.getCoupon());
        values.put(Constant.FLIGHT_COLUMN_IMAGE, flight.getImage());

        SQLiteDatabase db = getWritableDatabase();
        int result = db.update(Constant.FLIGHT_TABLE_NAME, values, Constant.FLIGHT_COLUMN_FLIGHT_ID + " = ?", new String[]{flight.getFlight_ID()});

        db.close();

        return result;
    }

}
