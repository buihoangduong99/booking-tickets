package com.example.bookingticketproject.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.activity.InfoBookingFlightActivity;
import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.database_handler.DatabaseHandlerCustomers;
import com.example.bookingticketproject.entity.BookingFlight;
import com.example.bookingticketproject.entity.Customer;
import com.example.bookingticketproject.entity.Flight;
import com.example.bookingticketproject.session.AccountSession;

import java.util.ArrayList;
import java.util.List;

public class FlightListAdapter extends ArrayAdapter<Flight> {

    private static final String TAG = "FlightListAdapter";
    String seatsClass;
    boolean roundTrip;
    int numberPassenger;

    private Context mContext;
    int mResource;

    DatabaseHandlerCustomers handlerCustomers;

    public FlightListAdapter(@NonNull Context context, int resource, List<Flight> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        handlerCustomers = new DatabaseHandlerCustomers(mContext, null, null, 1);
    }

    public FlightListAdapter(@NonNull Context context, int resource, List<Flight> objects, String seatsClass, boolean roundTrip, int numberPassenger) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        handlerCustomers = new DatabaseHandlerCustomers(mContext, null, null, 1);
        this.seatsClass = seatsClass;
        this.roundTrip = roundTrip;
        this.numberPassenger = numberPassenger;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //get the persons information
        String timeFlight = getItem(position).getGetTime_flight_from() + " - " + getItem(position).getGetTime_flight_to();
        String nameFlight = getItem(position).getBrand_flight();
        String price = getItem(position).getBusiness_price() + "";

        //Create the flight object with the information
//        Flight flight = new Flight(timeFlight, nameFlight, price);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView tvTimeFlight = (TextView) convertView.findViewById(R.id.timeFlight);
        TextView tvNameFlight = (TextView) convertView.findViewById(R.id.nameFlight);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.price);

        tvTimeFlight.setText(timeFlight);
        tvNameFlight.setText(nameFlight);
        tvPrice.setText(price);

        final AccountSession session = new AccountSession(mContext);

        Button btnBooking = convertView.findViewById(R.id.btnBooking);
        btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InfoBookingFlightActivity.class);
                Bundle bundle = new Bundle();
                Customer cus=handlerCustomers.findCustomerByAccountID(session.getAccountId());
                bundle.putInt(Constant.CUSTOMER_COLUMN_CUSTOMER_ID, cus.getCustomer_ID());
                bundle.putBoolean(Constant.BOOKING_FLIGHT_COLUMN_ROUND_TRIP, roundTrip);
                bundle.putString(Constant.FLIGHT_COLUMN_FLIGHT_ID,getItem(position).getFlight_ID());
                bundle.putInt(Constant.BOOKING_FLIGHT_COLUMN_PASSENGER, numberPassenger);
                bundle.putString(Constant.BOOKING_FLIGHT_COLUMN_SEAT_CLASS, seatsClass);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

}
