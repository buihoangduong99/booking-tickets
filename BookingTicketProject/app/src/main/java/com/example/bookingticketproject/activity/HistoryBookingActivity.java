package com.example.bookingticketproject.activity;


import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.adapter.HistoryBookingAdapter;
import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.database_handler.DatabaseHandlerBookingFlights;
import com.example.bookingticketproject.entity.BookingFlight;
import com.example.bookingticketproject.session.AccountSession;

import java.util.List;


public class HistoryBookingActivity extends AppCompatActivity {
    DatabaseHandlerBookingFlights handlerBookingFlights;
    ListView listFlightView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_booking);

        handlerBookingFlights = new DatabaseHandlerBookingFlights(this, Constant.DATABASE_NAME, null, 1);
        listFlightView = (ListView) findViewById(R.id.listBookingFlightView);

        AccountSession session = new AccountSession(this);
        List<BookingFlight> bookingFlights = handlerBookingFlights.findBookingFlightByAccountId(session.getAccountId());
        if (bookingFlights == null || bookingFlights.isEmpty()) {
            Toast.makeText(this, "No recent booking yet", Toast.LENGTH_SHORT).show();
        } else {
            HistoryBookingAdapter adapter = new HistoryBookingAdapter(this, R.layout.adapter_history_booking_layout, bookingFlights);
            listFlightView.setAdapter(adapter);
        }
    }
}