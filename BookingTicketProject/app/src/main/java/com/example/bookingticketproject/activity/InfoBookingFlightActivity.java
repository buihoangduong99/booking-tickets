package com.example.bookingticketproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.database_handler.DatabaseHandlerBookingFlights;
import com.example.bookingticketproject.database_handler.DatabaseHandlerCustomers;
import com.example.bookingticketproject.database_handler.DatabaseHandlerFlights;
import com.example.bookingticketproject.entity.BookingFlight;
import com.example.bookingticketproject.entity.Customer;
import com.example.bookingticketproject.entity.Flight;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InfoBookingFlightActivity extends AppCompatActivity {

    EditText txtFullName, txtDob, txtViewPhoneNumber, txtPrice, roadTrip, numberPassenger;
    TextView timeFlight, brandFlight, editTextAddressFrom, editTextAddressTo, dayFlight, seatsClass;

    Customer customer;
    BookingFlight bookingFlight;
    Flight flight;

    DatabaseHandlerFlights handlerFlights;
    DatabaseHandlerAccounts handlerAccounts;
    DatabaseHandlerCustomers handlerCustomers;
    DatabaseHandlerBookingFlights handlerBookingFlights;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_booking_flight);
        handlerFlights = new DatabaseHandlerFlights(this, null, null, 1);
        handlerAccounts = new DatabaseHandlerAccounts(this, null, null, 1);
        handlerCustomers = new DatabaseHandlerCustomers(this, null, null, 1);
        handlerBookingFlights = new DatabaseHandlerBookingFlights(this, null, null, 1);

        // Get views
        txtFullName = findViewById(R.id.txtFullName);
        txtDob = findViewById(R.id.txtDob);
        txtViewPhoneNumber = findViewById(R.id.txtViewPhoneNumber);
        txtPrice = findViewById(R.id.txtPrice);
        roadTrip = findViewById(R.id.roadTrip);
        timeFlight = findViewById(R.id.timeFlight);
        brandFlight = findViewById(R.id.brandFlight);
        editTextAddressFrom = findViewById(R.id.editTextAddressFrom);
        editTextAddressTo = findViewById(R.id.editTextAddressTo);
        dayFlight = findViewById(R.id.dayFlight);
        numberPassenger = (EditText)findViewById(R.id.numberPassenger);
        seatsClass = findViewById(R.id.seatsClass);

        // Call function to set content for view
        setValueFrame();
    }

    public void setValueFrame() {
        // Pre-define value
        boolean check_round_trip = false;
        int check_passenger = 1;
        String check_seat_class = "Economy";
        String flight_ID = null;
        int customer_ID = 0;

        // Get data passed from intent
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            customer_ID = bundle.getInt(Constant.CUSTOMER_COLUMN_CUSTOMER_ID);
            flight_ID = bundle.getString(Constant.FLIGHT_COLUMN_FLIGHT_ID);
            check_round_trip = bundle.getBoolean(Constant.BOOKING_FLIGHT_COLUMN_ROUND_TRIP);
            check_passenger = bundle.getInt(Constant.BOOKING_FLIGHT_COLUMN_PASSENGER);
            check_seat_class = bundle.getString(Constant.BOOKING_FLIGHT_COLUMN_SEAT_CLASS);
        }

        // Get data from Database
        customer = handlerCustomers.findCustomerByID(customer_ID);
        flight = handlerFlights.findFlightByID(flight_ID);

//        flight.setFirstclass_total(3);
//        flight.setFirstclass_price(2000000);
//        handlerFlights.updateFlight(flight);

        if ("Economy".equals(check_seat_class)) {
            flight.setEconomy_total(flight.getEconomy_total() - check_passenger);
        } else if ("Business".equals(check_seat_class)) {
            flight.setBusiness_total(flight.getBusiness_total() - check_passenger);
        } else if ("First Class".equals(check_seat_class)) {
            flight.setFirstclass_total(flight.getFirstclass_total() - check_passenger);
        }

        flight.setImage(null);

        //fake data
//        fli= new Flight("VJ001","VietJet","Ha Noi","HCM","11-11-2020","16:00 PM","17:50 PM",4,1.7f,4,1.5f,3,2,0,null);
//        cus = new Customer(1, "Nguyen Mai Tien", "Hung Yen", "25/07/1999", false, "0912311234", 1);

        bookingFlight = new BookingFlight(customer.getAccount_ID(), flight.getFlight_ID(), check_round_trip, check_passenger, check_seat_class, new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

        // Setting value for fields on view
        txtFullName.setText(customer.getCustomer_fullname());
        txtDob.setText(customer.getCustomer_dob());
        txtViewPhoneNumber.setText(customer.getCustomer_phonenumber());
        editTextAddressFrom.setText(flight.getAddress_from());
        editTextAddressTo.setText(flight.getAddress_to());
        dayFlight.setText(flight.getDeparture_day());
        timeFlight.setText(flight.getGetTime_flight_from() + " - " + flight.getGetTime_flight_to());
        brandFlight.setText(flight.getBrand_flight());
        roadTrip.setText(check_round_trip ? "Round trip" : "One way");
        numberPassenger.setText(String.valueOf(check_passenger));
        seatsClass.setText(check_seat_class);
        txtPrice.setText(getTotalPrice(flight, bookingFlight));

    }

    public String getTotalPrice(Flight fli, BookingFlight bkf) {
        float result = 1;

        if (bkf.getSeat_class().equals("Economy")) {
            result = fli.getEconomy_price();
        } else if (bkf.getSeat_class().equals("Business class")) {
            result = fli.getBusiness_price();
        } else {
            result = fli.getFirstclass_price();
        }

        if (bkf.isRound_trip()) {
            result = result * 2;
        }

        result = result * (100 - fli.getCoupon()) * bkf.getPassenger() / 100;

        return (result + " triệu đồng ");
    }

    public void onClick(View view) {
        Long resultBkf = handlerBookingFlights.insertBookingFlight(bookingFlight);
        Toast.makeText(this, "Successfully", Toast.LENGTH_LONG).show();

        handlerFlights.updateFlight(flight);

        // Back to home page
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);

    }
}