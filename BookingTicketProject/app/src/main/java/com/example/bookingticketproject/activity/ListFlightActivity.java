package com.example.bookingticketproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.adapter.FlightListAdapter;
import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.database_handler.DatabaseHandlerFlights;
import com.example.bookingticketproject.entity.Flight;

import java.util.List;

public class ListFlightActivity extends AppCompatActivity {

    DatabaseHandlerFlights handlerfli;
//    DatabaseHandlerCustomers handlerCustomers;

    private static final String TAG = "BookingFlightActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_flight);

        // Initialize database handler
        handlerfli = new DatabaseHandlerFlights(this, null, null, 1);

        // Get views
        ListView listFlightView = (ListView) findViewById(R.id.listView);

        // Get data from intent bundle
        boolean check_round_trip = false;
        int check_passenger = 1;
        String check_seat_class = "Economy", check_depart_day = "";
        String address_From = "";
        String address_To = "";

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            address_From = bundle.getString(Constant.FLIGHT_COLUMN_ADDRESS_FROM);
            address_To = bundle.getString(Constant.FLIGHT_COLUMN_ADDRESS_TO);
            check_round_trip = bundle.getBoolean(Constant.BOOKING_FLIGHT_COLUMN_ROUND_TRIP);
            check_passenger = bundle.getInt(Constant.BOOKING_FLIGHT_COLUMN_PASSENGER);
            check_seat_class = bundle.getString(Constant.BOOKING_FLIGHT_COLUMN_SEAT_CLASS);
            check_depart_day = bundle.getString(Constant.FLIGHT_COLUMN_DEPARTURE_DAY);
        }

        // Get list of flight satisfies data
        List<Flight> listFlights = null;
        listFlights = handlerfli.searchListFlightWithBookInfo(address_From, address_To, check_depart_day, check_seat_class, check_passenger);

        if (listFlights == null) {
            Toast.makeText(this, "No flight found!", Toast.LENGTH_SHORT).show();
        } else {
            // Display list
            FlightListAdapter adapter = new FlightListAdapter(this, R.layout.adapter_view_layout, listFlights, check_seat_class, check_round_trip, check_passenger);
            listFlightView.setAdapter(adapter);
        }

    }
}