package com.example.bookingticketproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.activity.FlightManagementActivity;
import com.example.bookingticketproject.activity.HomeActivity;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.session.AccountSession;

public class AdminManageActivity extends AppCompatActivity {

    AccountSession session;
    DatabaseHandlerAccounts handlerAccounts;
    TextView textViewEmailDisplay;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_manage);

        session = new AccountSession(this);

        handlerAccounts = new DatabaseHandlerAccounts(this, null, null, 1);

        // Get views
        textViewEmailDisplay = findViewById(R.id.textViewEmailDisplay);

        // Setting values
        textViewEmailDisplay.setText((handlerAccounts.findAccountByID(session.getAccountId())).getAccount_email());

    }

    public void btnManageAccountOnClick(View view) {
        Intent intent = new Intent(this, AccountManagementActivity.class);
        startActivity(intent);
    }

    public void btnManageFlightOnClick(View view) {
        Intent intent = new Intent(this, FlightManagementActivity.class);
        startActivity(intent);
    }

    public void btnLogoutOnClick(View view) {
        // Update session
        session.setAccountId(0);

        // Show Toast
        Toast.makeText(this, "Logged out", Toast.LENGTH_SHORT).show();

        // Back to home page
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

}