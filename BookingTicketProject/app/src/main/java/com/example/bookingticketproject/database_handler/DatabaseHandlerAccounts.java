package com.example.bookingticketproject.database_handler;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.entity.Account;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandlerAccounts extends SQLiteOpenHelper {

    ContentResolver resolver;

    public DatabaseHandlerAccounts(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, Constant.DATABASE_NAME, factory, Constant.VERSION);
        resolver = context.getContentResolver();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constant.CREATE_TABLE_ACCOUNT);
        db.execSQL(Constant.CREATE_TABLE_BOOKING_FLIGHT);
        db.execSQL(Constant.CREATE_TABLE_CUSTOMER);
        db.execSQL(Constant.CREATE_TABLE_FLIGHT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + Constant.ACCOUNT_TABLE_NAME);
        onCreate(db);
    }

    public Long insertAccount(Account account) {
        ContentValues values = new ContentValues();
        values.put(Constant.ACCOUNT_COLUMN_ACCOUNT_EMAIL, account.getAccount_email());
        values.put(Constant.ACCOUNT_COLUMN_ACCOUNT_PASSWORD, account.getAccount_password());
        values.put(Constant.ACCOUNT_COLUMN_ACCOUNT_ROLE, account.getAccount_role());
        values.put(Constant.ACCOUNT_COLUMN_STATUS, account.isStatus());

        SQLiteDatabase db = getWritableDatabase();
        Long result = db.insert(Constant.ACCOUNT_TABLE_NAME, null, values);

        db.close();

        return result;
    }

    public List<Account> getAllCustomerAccount() {
        List<Account> accounts = new ArrayList<>();
        String query = " SELECT * FROM " + Constant.ACCOUNT_TABLE_NAME + " WHERE " + Constant.ACCOUNT_COLUMN_ACCOUNT_ROLE + " = 'user'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Account account = new Account();
                account.setAccount_ID(cursor.getInt(0));
                account.setAccount_email(cursor.getString(1));
                account.setAccount_password(cursor.getString(2));
                account.setAccount_role(cursor.getString(3));
                account.setStatus("1".equals(cursor.getString(4)) ? true : false);
                accounts.add(account);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return accounts;
        }
        cursor.close();
        db.close();
        return null;
    }

    public List<Account> getAllCustomerAccountLikeEmail(String email) {
        List<Account> accounts = new ArrayList<>();
        String query = " SELECT * FROM " + Constant.ACCOUNT_TABLE_NAME + " WHERE " + Constant.ACCOUNT_COLUMN_ACCOUNT_EMAIL + " LIKE '%" + email + "%'" + " AND " + Constant.ACCOUNT_COLUMN_ACCOUNT_ROLE + " = 'user'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Account account = new Account();
                account.setAccount_ID(cursor.getInt(0));
                account.setAccount_email(cursor.getString(1));
                account.setAccount_password(cursor.getString(2));
                account.setAccount_role(cursor.getString(3));
                account.setStatus("1".equals(cursor.getString(4)) ? true : false);
                accounts.add(account);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return accounts;
        }
        cursor.close();
        db.close();
        return null;
    }

    public List<Account> getAllLockedAccount() {
        List<Account> accounts = new ArrayList<>();
        String query = " SELECT * FROM " + Constant.ACCOUNT_TABLE_NAME + " WHERE " + Constant.ACCOUNT_COLUMN_STATUS + " = 0";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Account account = new Account();
                account.setAccount_ID(cursor.getInt(0));
                account.setAccount_email(cursor.getString(1));
                account.setAccount_password(cursor.getString(2));
                account.setAccount_role(cursor.getString(3));
                account.setStatus("1".equals(cursor.getString(4)) ? true : false);
                accounts.add(account);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return accounts;
        }
        cursor.close();
        db.close();
        return null;
    }

    public Account findAccountByID(int id) {
        String query = " SELECT * FROM " + Constant.ACCOUNT_TABLE_NAME + " WHERE " + Constant.ACCOUNT_COLUMN_ACCOUNT_ID + " = '" + id + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            Account account = new Account();
            account.setAccount_ID(cursor.getInt(0));
            account.setAccount_email(cursor.getString(1));
            account.setAccount_password(cursor.getString(2));
            account.setAccount_role(cursor.getString(3));
            account.setStatus("1".equals(cursor.getString(4)) ? true : false);
            cursor.close();
            db.close();
            return account;
        }
        cursor.close();
        db.close();
        return null;
    }

    public Account findAccountByEmail(String email) {
        String query = " SELECT * FROM " + Constant.ACCOUNT_TABLE_NAME + " WHERE " + Constant.ACCOUNT_COLUMN_ACCOUNT_EMAIL + " = '" + email + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            Account account = new Account();
            account.setAccount_ID(cursor.getInt(0));
            account.setAccount_email(cursor.getString(1));
            account.setAccount_password(cursor.getString(2));
            account.setAccount_role(cursor.getString(3));
            account.setStatus("1".equals(cursor.getString(4)) ? true : false);
            cursor.close();
            db.close();
            return account;
        }
        cursor.close();
        db.close();
        return null;
    }

    public Account findAccountByEmailPassword(String email, String password) {
        String query = " SELECT * FROM " + Constant.ACCOUNT_TABLE_NAME + " WHERE " + Constant.ACCOUNT_COLUMN_ACCOUNT_EMAIL + " = '" + email + "'" + " AND " + Constant.ACCOUNT_COLUMN_ACCOUNT_PASSWORD + " ='" + password + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            Account account = new Account();
            account.setAccount_ID(cursor.getInt(0));
            account.setAccount_email(cursor.getString(1));
            account.setAccount_password(cursor.getString(2));
            account.setAccount_role(cursor.getString(3));
            account.setStatus("1".equals(cursor.getString(4)) ? true : false);
            cursor.close();
            db.close();
            return account;
        }
        cursor.close();
        db.close();
        return null;
    }

    public int updateAccountPasswordById(int accountId, String password) {
        Account account = findAccountByID(accountId);
        if (account == null) {
            return 0;
        } else {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Constant.ACCOUNT_COLUMN_ACCOUNT_PASSWORD, password);
            int result = db.update(Constant.ACCOUNT_TABLE_NAME, values, Constant.ACCOUNT_COLUMN_ACCOUNT_ID + " = ?", new String[]{accountId + ""});
            db.close();
            return result;
        }
    }

    public int updateAccountStatusById(int accountId, boolean status) {
        Account account = findAccountByID(accountId);
        if (account == null)
            return 0;
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(Constant.ACCOUNT_COLUMN_STATUS, status);
        int result = db.update(Constant.ACCOUNT_TABLE_NAME, cv, Constant.ACCOUNT_COLUMN_ACCOUNT_ID + " = ?", new String[]{accountId + ""});
        db.close();
        return result;
    }

}
