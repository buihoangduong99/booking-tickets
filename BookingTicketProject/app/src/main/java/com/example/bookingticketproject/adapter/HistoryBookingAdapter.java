package com.example.bookingticketproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.database_handler.DatabaseHandlerBookingFlights;
import com.example.bookingticketproject.database_handler.DatabaseHandlerFlights;
import com.example.bookingticketproject.entity.BookingFlight;
import com.example.bookingticketproject.entity.Flight;

import java.util.List;

public class HistoryBookingAdapter extends ArrayAdapter<BookingFlight> {

    private Context mContext;
    int mResource;

    public HistoryBookingAdapter(@NonNull Context context, int resource, List<BookingFlight> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //get the History booking information
        String flight_ID = getItem(position).getFlight_ID();
        boolean round_trip = getItem(position).isRound_trip();

        String round_strip = "One way";
        if (round_trip) {
            round_strip = "Round trip";
        }

        Flight flight = new DatabaseHandlerFlights(mContext, null, null, 1).findFlightByID(flight_ID);
        BookingFlight bookingFlight = new DatabaseHandlerBookingFlights(mContext, null, null, 1)
                .findBookingFlightByFlightIdAndAccountId(getItem(position).getAccount_ID(), flight_ID);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        // History booking
        TextView txtFlightID = (TextView) convertView.findViewById(R.id.txtflightID);
        TextView typeTrip = (TextView) convertView.findViewById(R.id.typetrip);

        // Flight
        TextView txtBranch = (TextView) convertView.findViewById(R.id.txtbranch);
        TextView txtAddress = (TextView) convertView.findViewById(R.id.txtaddress);
        TextView txtDay = (TextView) convertView.findViewById(R.id.txtday);
        TextView txtPrice = (TextView) convertView.findViewById(R.id.txtprice);
        TextView numberPassenger = convertView.findViewById(R.id.numberpassenger);
        TextView txtSeatClass = convertView.findViewById(R.id.seatclasss);

        // Set Text
        txtBranch.setText(flight.getBrand_flight());
        txtAddress.setText(flight.getAddress_from() + " - " + flight.getAddress_to());
        txtDay.setText(flight.getDeparture_day());
        numberPassenger.setText(bookingFlight.getPassenger() + "");
        txtPrice.setText(getTotalPrice(flight, getItem(position)));
        txtSeatClass.setText(bookingFlight.getSeat_class());

        txtFlightID.setText(flight_ID);
        typeTrip.setText(round_strip);

        return convertView;
    }

    public String getTotalPrice(Flight fli, BookingFlight bkf) {
        float result = 1;

        if (bkf.getSeat_class().compareTo("Economy") == 0) result = fli.getEconomy_price();
        else if (bkf.getSeat_class().compareTo("Business class") == 0)
            result = fli.getBusiness_price();
        else if (bkf.getSeat_class().compareTo("First class") == 0)
            result = fli.getBusiness_price();

        if (bkf.isRound_trip()) result = result * 2;

        result = result * (100 - fli.getCoupon()) * bkf.getPassenger() / 100;

        return (result + "");
    }
}