package com.example.bookingticketproject.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.database_handler.DatabaseHandlerCustomers;
import com.example.bookingticketproject.entity.Customer;

import java.util.Calendar;

public class EditProfileDialog extends Dialog implements android.view.View.OnClickListener {

    Activity activity;
    Customer customer;

    public EditProfileDialog(Activity activity, Customer customer) {
        super(activity);
        this.activity = activity;
        this.customer = customer;
    }

    EditText textViewFullNameDisplay;
    EditText textViewDobDisplay;
    RadioButton radio_male;
    RadioButton radio_female;
    EditText textViewPhoneNumberDisplay;
    EditText textViewAddressDisplay;
    TextView txtErrorMsg;

    Button btnSubmit;

    DatabaseHandlerCustomers handlerCustomers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_profile);

        // Get the text views of screen
        textViewFullNameDisplay = findViewById(R.id.textViewFullNameDisplay);
        textViewDobDisplay = findViewById(R.id.textViewDobDisplay);
        radio_male = findViewById(R.id.radio_male);
        radio_female = findViewById(R.id.radio_female);
        textViewPhoneNumberDisplay = findViewById(R.id.textViewPhoneNumberDisplay);
        textViewAddressDisplay = findViewById(R.id.textViewAddressDisplay);
        txtErrorMsg = findViewById(R.id.txtErrorMsg);

        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        textViewDobDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        // Set the text views to corresponding data
        textViewFullNameDisplay.setText(customer.getCustomer_fullname());
        textViewDobDisplay.setText(customer.getCustomer_dob());
        if (customer.isCustomer_gender()) {
            radio_male.setSelected(true);
            radio_female.setSelected(false);
        } else {
            radio_female.setSelected(true);
            radio_male.setSelected(false);
        }
        textViewPhoneNumberDisplay.setText(customer.getCustomer_phonenumber());
        textViewAddressDisplay.setText(customer.getCustomer_address());

        // Initiate the database handler
        handlerCustomers = new DatabaseHandlerCustomers(this.getContext(), null, null, 1);

    }

    @Override
    public void onClick(View v) {
        if (!isBlankInformation(textViewFullNameDisplay, textViewDobDisplay, textViewPhoneNumberDisplay, textViewAddressDisplay)) {
            Customer customer = this.customer;
            customer.setCustomer_fullname(textViewFullNameDisplay.getText().toString());
            customer.setCustomer_address(textViewAddressDisplay.getText().toString());
            customer.setCustomer_dob(textViewDobDisplay.getText().toString());
            customer.setCustomer_gender(radio_male.isChecked() ? true : false);
            customer.setCustomer_phonenumber(textViewPhoneNumberDisplay.getText().toString());

            handlerCustomers.updateCustomerByCustomerId(customer);

            txtErrorMsg.setText("");
            Toast.makeText(this.getContext(), "Your information has been edited!", Toast.LENGTH_SHORT).show();
            dismiss();
        } else {
            txtErrorMsg.setText("*Information cannot be blank!*");
        }
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this.getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                textViewDobDisplay.setText(dayOfMonth + "/" + ((month + 1) < 10 ? "0" + (month + 1) : (month + 1)) + "/" + year);
            }
        }, year, month, day);

        dialog.show();
    }

    private boolean isBlankInformation(EditText... editTexts) {
        for (EditText editText: editTexts) {
            if (editText == null || ("").equals(editText.getText().toString())) {
                return true;
            }
        }
        return false;
    }

}