package com.example.bookingticketproject.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.adapter.ListAccountManageAdapter;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.entity.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountManagementActivity extends AppCompatActivity {

    EditText txtEmail;
    ListView listViewAccounts;

    DatabaseHandlerAccounts handlerAccounts;

    List<Account> accounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_management);

        // Get views
        txtEmail = findViewById(R.id.txtEmail);
        listViewAccounts = findViewById(R.id.listViewAccounts);

        // Initialize database
        handlerAccounts = new DatabaseHandlerAccounts(this, null, null, 1);
        accounts = handlerAccounts.getAllCustomerAccount();
        if (isNullOrEmptyList(accounts)) {
            accounts = new ArrayList<>();
        }
        // Set adapter for list view
        listViewAccounts.setAdapter(new ListAccountManageAdapter(this, accounts));

    }

    public void btnViewLockedUsersOnClick(View view) {
        accounts = handlerAccounts.getAllLockedAccount();
        if (isNullOrEmptyList(accounts)) {
            accounts = new ArrayList<>();
        }
        // Set adapter for list view
        listViewAccounts.setAdapter(new ListAccountManageAdapter(this, accounts));

    }

    public void btnSearchOnClick(View view) {
        // Set adapter for list view
        if (txtEmail.getText().toString() == null || "".equals(txtEmail.getText().toString())) {
            accounts = handlerAccounts.getAllCustomerAccount();
        } else {
            accounts = handlerAccounts.getAllCustomerAccountLikeEmail(txtEmail.getText().toString());
        }
        if (isNullOrEmptyList(accounts)) {
            accounts = new ArrayList<>();
        }
        // Set adapter for list view
        listViewAccounts.setAdapter(new ListAccountManageAdapter(this, accounts));
    }

    public boolean isNullOrEmptyList(List<Account> accounts) {
        if (accounts == null || accounts.isEmpty()) {
            Toast.makeText(this, "No account found!", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }
    }

}