package com.example.bookingticketproject.database_handler;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.entity.BookingFlight;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandlerBookingFlights extends SQLiteOpenHelper {

    ContentResolver resolver;

    public DatabaseHandlerBookingFlights(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, Constant.DATABASE_NAME, factory, Constant.VERSION);
        resolver = context.getContentResolver();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constant.CREATE_TABLE_ACCOUNT);
        db.execSQL(Constant.CREATE_TABLE_BOOKING_FLIGHT);
        db.execSQL(Constant.CREATE_TABLE_CUSTOMER);
        db.execSQL(Constant.CREATE_TABLE_FLIGHT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + Constant.BOOKING_FLIGHT_TABLE_NAME);
        onCreate(db);
    }

    public Long insertBookingFlight(BookingFlight bookingFlight) {
        ContentValues values = new ContentValues();
        values.put(Constant.BOOKING_FLIGHT_COLUMN_ACCOUNT_ID, bookingFlight.getAccount_ID());
        values.put(Constant.BOOKING_FLIGHT_COLUMN_FLIGHT_ID, bookingFlight.getFlight_ID());
        values.put(Constant.BOOKING_FLIGHT_COLUMN_ROUND_TRIP, bookingFlight.isRound_trip());
        values.put(Constant.BOOKING_FLIGHT_COLUMN_PASSENGER, bookingFlight.getPassenger());
        values.put(Constant.BOOKING_FLIGHT_COLUMN_SEAT_CLASS, bookingFlight.getSeat_class());
        values.put(Constant.BOOKING_FLIGHT_COLUMN_BOOKING_DATE, bookingFlight.getBooking_date());

        SQLiteDatabase db = getWritableDatabase();
        Long result = db.insert(Constant.BOOKING_FLIGHT_TABLE_NAME, null, values);

        db.close();

        return result;
    }

    public List<BookingFlight> getAllBookingFlight() {
        List<BookingFlight> bookFlights = new ArrayList<>();
        String query = " SELECT * FROM " + Constant.BOOKING_FLIGHT_TABLE_NAME;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                BookingFlight bookFlight = new BookingFlight();
                bookFlight.setAccount_ID(cursor.getInt(0));
                bookFlight.setFlight_ID(cursor.getString(1));
                bookFlight.setRound_trip("1".equals(cursor.getString(2)) ? true : false);
                bookFlight.setPassenger(cursor.getInt(3));
                bookFlight.setSeat_class(cursor.getString(4));
                bookFlight.setBooking_date(cursor.getString(5));

                bookFlights.add(bookFlight);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return bookFlights;
        }
        cursor.close();
        db.close();
        return null;
    }

    public BookingFlight findBookingFlightByFlightIdAndAccountId(int accId, String flightId) {
        String query = " SELECT * FROM " + Constant.BOOKING_FLIGHT_TABLE_NAME + " WHERE " + Constant.BOOKING_FLIGHT_COLUMN_ACCOUNT_ID + " ='" + accId + "'" + " AND " + Constant.BOOKING_FLIGHT_COLUMN_FLIGHT_ID + " = '" + flightId + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            BookingFlight bookFlight = new BookingFlight();
            bookFlight.setAccount_ID(cursor.getInt(0));
            bookFlight.setFlight_ID(cursor.getString(1));
            bookFlight.setRound_trip("1".equals(cursor.getString(2)) ? true : false);
            bookFlight.setPassenger(cursor.getInt(3));
            bookFlight.setSeat_class(cursor.getString(4));
            bookFlight.setBooking_date(cursor.getString(5));
            cursor.close();
            db.close();
            return bookFlight;
        }
        cursor.close();
        db.close();
        return null;
    }

    public List<BookingFlight> findBookingFlightByAccountId(int accId) {

        String query = " SELECT * FROM " + Constant.BOOKING_FLIGHT_TABLE_NAME + " WHERE " + Constant.BOOKING_FLIGHT_COLUMN_ACCOUNT_ID + " ='" + accId +"'";
        SQLiteDatabase db = getReadableDatabase();
        List<BookingFlight> bookFlights = new ArrayList<>();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                BookingFlight bookFlight = new BookingFlight();
                bookFlight.setAccount_ID(cursor.getInt(0));
                bookFlight.setFlight_ID(cursor.getString(1));
                bookFlight.setRound_trip("1".equals(cursor.getString(2)) ? true : false);
                bookFlight.setPassenger(cursor.getInt(3));
                bookFlight.setSeat_class(cursor.getString(4));
                bookFlight.setBooking_date(cursor.getString(5));

                bookFlights.add(bookFlight);
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return bookFlights;
        }
        cursor.close();
        db.close();
        return null;
    }

}
