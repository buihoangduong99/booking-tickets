package com.example.bookingticketproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.database_handler.DatabaseHandlerBookingFlights;
import com.example.bookingticketproject.database_handler.DatabaseHandlerCustomers;
import com.example.bookingticketproject.database_handler.DatabaseHandlerFlights;
import com.example.bookingticketproject.entity.Account;
import com.example.bookingticketproject.entity.Customer;
import com.example.bookingticketproject.entity.Flight;
import com.example.bookingticketproject.session.AccountSession;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgIcon;
    TextView textViewBookingTicketTitle;
    TextView textViewBookingTicket;

    EditText txtEmailAddress;
    Button btnViewInfoLogin;
    AccountSession session;

    DatabaseHandlerAccounts handlerAccounts;


    DatabaseHandlerCustomers handlerCustomers;
    DatabaseHandlerFlights handlerFlights;
    DatabaseHandlerBookingFlights handlerBookingFlights;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // For handle event
        imgIcon = findViewById(R.id.imgIcon);
        textViewBookingTicketTitle = findViewById(R.id.textViewBookingTicketTitle);
        textViewBookingTicket = findViewById(R.id.textViewBookingTicket);

        // Event onclick
        imgIcon.setOnClickListener(this);
        textViewBookingTicketTitle.setOnClickListener(this);
        textViewBookingTicketTitle.setOnClickListener(this);

        txtEmailAddress = findViewById(R.id.txtEmailAddress);
        btnViewInfoLogin = findViewById(R.id.btnViewInfoLogin);

        handlerAccounts = new DatabaseHandlerAccounts(this, null, null, 1);
//        handlerCustomers = new DatabaseHandlerCustomers(this, null, null, 1);
//        handlerFlights = new DatabaseHandlerFlights(this, null, null, 1);
//        handlerBookingFlights = new DatabaseHandlerBookingFlights(this, null, null, 1);
//
//        handlerAccounts.insertAccount(new Account("admin@gmail.com", "123", "admin", true));
//        handlerAccounts.insertAccount(new Account("anv@gmail.com", "123", "user", true));
//        handlerAccounts.insertAccount(new Account("bnt@gmail.com", "123", "user", true));
//
//        handlerCustomers.insertCustomers(new Customer("Nguyen Van A", "Ha Noi", "15/08/1995", true, "0123456789", 2));
//        handlerCustomers.insertCustomers(new Customer("Nguyen Thi B", "Ha Noi", "13/11/1996", false, "0987654321", 3));
//
//        handlerFlights.insertFlight(new Flight("VJ001","VietJet","Ha Noi","Da Nang","15/11/2020","16:00 PM","17:50 PM",4,1700000,4,1500000,3,2000000,0,null));

        session = new AccountSession(this);
        if (validateSession(session)) { // Logged in
            // -> Change button to icon user
            btnViewInfoLogin.setText("");

            // Resize button
            ViewGroup.LayoutParams layoutParams = btnViewInfoLogin.getLayoutParams();
            layoutParams.width = 150;
            btnViewInfoLogin.setLayoutParams(layoutParams);

            // Set button background to image
            btnViewInfoLogin.setBackgroundResource(R.drawable.iconuser);

            // Programmatically add email to the "contact us" section
            txtEmailAddress.setText(handlerAccounts.findAccountByID(session.getAccountId()).getAccount_email());
            // Then lock the focusable of the "contact us" section
            txtEmailAddress.setFocusable(false);
        } else { // Not logged in
            // Change text of button to login
            btnViewInfoLogin.setText(R.string.login_button_text);
        }
    }

    public void btnViewInfoLoginOnClick(View view) {
        Intent intent = null;
        if (validateSession(session)) {
            Account account = handlerAccounts.findAccountByID(session.getAccountId());
            if ("admin".equals(account.getAccount_role())) {
                intent = new Intent(this, AdminManageActivity.class);
            } else if ("user".equals(account.getAccount_role())) {
                intent = new Intent(this, DisplayProfileActivity.class);
            }
        } else {
            intent = new Intent(this, LoginActivity.class);
        }
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, SearchFlightActivity.class);
        startActivity(intent);
    }

    private boolean validateSession(AccountSession session) {
        if (session.getAccountId() == 0) {
            return false;
        } else {
            return true;
        }
    }
}
