package com.example.bookingticketproject.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.activity.HomeActivity;
import com.example.bookingticketproject.database_handler.DatabaseHandlerAccounts;
import com.example.bookingticketproject.entity.Account;

public class ForgotPasswordDialog extends Dialog implements View.OnClickListener {

    Activity activity;

    public ForgotPasswordDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    EditText txtEmail;
    EditText txtPassword;
    EditText txtPasswordConfirm;
    TextView txtErrorMsg;

    Button btnSubmit;

    DatabaseHandlerAccounts handlerAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_forgot_password);

        // Get all the views
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        txtPasswordConfirm = findViewById(R.id.txtPasswordConfirm);
        txtErrorMsg = findViewById(R.id.txtErrorMsg);

        btnSubmit = findViewById(R.id.btnSubmit);

        // Handle event onClick for button
        btnSubmit.setOnClickListener(this);

        // Initiate the database handler
        handlerAccounts = new DatabaseHandlerAccounts(this.getContext(), null, null, 1);
    }

    @Override
    public void onClick(View v) {
        // Check blank input
        if (!isBlankInformation(txtPassword, txtPasswordConfirm)) {
            String email = txtEmail.getText().toString();
            Account account = handlerAccounts.findAccountByEmail(email);
            if (account != null) {
                // Get data input
                String password = txtPassword.getText().toString();
                String passwordConfirm = txtPasswordConfirm.getText().toString();

                // If match password
                if (password.equals(passwordConfirm)) {
                    // Change password
                    int result = handlerAccounts.updateAccountPasswordById(account.getAccount_ID(), password);
                    // TODO Check error occur when update(later)
                    txtErrorMsg.setText("");
                    Toast.makeText(this.getContext(), "Password changed!", Toast.LENGTH_SHORT).show();

                    // Back to home page
                    Intent intent = new Intent(this.getContext(), HomeActivity.class);
                    this.getContext().startActivity(intent);

                    dismiss();
                } else { // If not match password
                    txtErrorMsg.setText("*Password and confirmation does not match!*");
                }
            } else {
                txtErrorMsg.setText("*Email have not registered or does'nt exist!*");
            }
        } else {
            txtErrorMsg.setText("*Must fill in all information!*");
        }
    }

    private boolean isBlankInformation(EditText... editTexts) {
        for (EditText editText : editTexts) {
            if (editText == null || ("").equals(editText.getText().toString())) {
                return true;
            }
        }
        return false;
    }
}