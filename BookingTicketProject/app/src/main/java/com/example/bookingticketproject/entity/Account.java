package com.example.bookingticketproject.entity;

public class Account {
    private int account_ID;
    private String account_email;
    private String account_password;
    private String account_role;
    private boolean status;

    public Account() {
    }
    public Account(int account_ID, String account_email, String account_password, String account_role, boolean status) {
        this.account_ID = account_ID;
        this.account_email = account_email;
        this.account_password = account_password;
        this.account_role = account_role;
        this.status = status;
    }

    public Account(String account_email, String account_password, String account_role, boolean status) {
        this.account_email = account_email;
        this.account_password = account_password;
        this.account_role = account_role;
        this.status = status;
    }

    public int getAccount_ID() {
        return account_ID;
    }

    public void setAccount_ID(int account_ID) {
        this.account_ID = account_ID;
    }

    public String getAccount_email() {
        return account_email;
    }

    public void setAccount_email(String account_email) {
        this.account_email = account_email;
    }

    public String getAccount_password() {
        return account_password;
    }

    public void setAccount_password(String account_password) {
        this.account_password = account_password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAccount_role() {
        return account_role;
    }

    public void setAccount_role(String account_role) {
        this.account_role = account_role;
    }
}
