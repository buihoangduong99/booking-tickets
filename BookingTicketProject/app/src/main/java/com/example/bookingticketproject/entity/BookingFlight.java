package com.example.bookingticketproject.entity;

public class BookingFlight {
    private int account_ID;
    private String flight_ID;
    private boolean round_trip;
    private int passenger;
    private String seat_class;
    private String booking_date;

    public BookingFlight() {
    }

    public BookingFlight(int account_ID, String flight_ID, boolean round_trip, int passenger, String seat_class, String booking_date) {
        this.account_ID = account_ID;
        this.flight_ID = flight_ID;
        this.round_trip = round_trip;
        this.passenger = passenger;
        this.seat_class = seat_class;
        this.booking_date = booking_date;
    }

    public int getAccount_ID() {
        return account_ID;
    }

    public void setAccount_ID(int account_ID) {
        this.account_ID = account_ID;
    }

    public String getFlight_ID() {
        return flight_ID;
    }

    public void setFlight_ID(String flight_ID) {
        this.flight_ID = flight_ID;
    }

    public boolean isRound_trip() {
        return round_trip;
    }

    public void setRound_trip(boolean round_trip) {
        this.round_trip = round_trip;
    }

    public int getPassenger() {
        return passenger;
    }

    public void setPassenger(int passenger) {
        this.passenger = passenger;
    }

    public String getSeat_class() {
        return seat_class;
    }

    public void setSeat_class(String seat_class) {
        this.seat_class = seat_class;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }
}
