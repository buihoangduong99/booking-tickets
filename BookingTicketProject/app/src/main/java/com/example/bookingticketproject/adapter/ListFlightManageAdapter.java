package com.example.bookingticketproject.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookingticketproject.R;
import com.example.bookingticketproject.activity.DisplayProfileActivity;
import com.example.bookingticketproject.activity.EditProfileDialog;
import com.example.bookingticketproject.activity.FlightManagementActivity;
import com.example.bookingticketproject.activity.ListFlightActivity;
import com.example.bookingticketproject.activity.SaveFlightActivity;
import com.example.bookingticketproject.constant.Constant;
import com.example.bookingticketproject.entity.Flight;

import java.util.List;

public class ListFlightManageAdapter extends BaseAdapter {

    List<Flight> flightList;
    Context context;

    private static LayoutInflater inflater = null;

    public ListFlightManageAdapter(FlightManagementActivity flightManagementActivity, List<Flight> flightList) {
        this.flightList = flightList;
        context = flightManagementActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return flightList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ListFlightManageHolder holder = new ListFlightManageHolder();
        View rowView;
        rowView = inflater.inflate(R.layout.adapter_list_flight, null);
        holder.textViewTime = rowView.findViewById(R.id.timeFlight);
        holder.textViewBrand = rowView.findViewById(R.id.nameFlight);
        holder.textViewPrice = rowView.findViewById(R.id.price);

        final Flight flight = flightList.get(position);
        holder.textViewTime.setText(flight.getGetTime_flight_from() + " - " + flight.getGetTime_flight_to());
        holder.textViewBrand.setText(flight.getBrand_flight());
        holder.textViewPrice.setText(flight.getEconomy_price() + "" + " triệu đồng");
        //
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "Flight: " + flight.getBrand_flight(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, SaveFlightActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constant.FLIGHT_COLUMN_FLIGHT_ID, flight.getFlight_ID());
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
        return rowView;
    }


    class ListFlightManageHolder {
        public TextView textViewTime;
        public TextView textViewBrand;
        public TextView textViewPrice;
    }

}
