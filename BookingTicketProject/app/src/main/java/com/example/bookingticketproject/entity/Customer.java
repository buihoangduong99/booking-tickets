package com.example.bookingticketproject.entity;

public class Customer {
    private int customer_ID;
    private String customer_fullname;
    private String customer_address;
    private String customer_dob;
    private boolean customer_gender;
    private String customer_phonenumber;
    private int account_ID;

    public Customer() {
    }

    public Customer(int customer_ID, String customer_fullname, String customer_address, String customer_dob, boolean customer_gender, String customer_phonenumber, int account_ID) {
        this.customer_ID = customer_ID;
        this.customer_fullname = customer_fullname;
        this.customer_address = customer_address;
        this.customer_dob = customer_dob;
        this.customer_gender = customer_gender;
        this.customer_phonenumber = customer_phonenumber;
        this.account_ID = account_ID;
    }

    public Customer(String customer_fullname, String customer_address, String customer_dob, boolean customer_gender, String customer_phonenumber, int account_ID) {
        this.customer_fullname = customer_fullname;
        this.customer_address = customer_address;
        this.customer_dob = customer_dob;
        this.customer_gender = customer_gender;
        this.customer_phonenumber = customer_phonenumber;
        this.account_ID = account_ID;
    }

    public int getCustomer_ID() {
        return customer_ID;
    }

    public void setCustomer_ID(int customer_ID) {
        this.customer_ID = customer_ID;
    }

    public String getCustomer_fullname() {
        return customer_fullname;
    }

    public void setCustomer_fullname(String customer_fullname) {
        this.customer_fullname = customer_fullname;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_dob() {
        return customer_dob;
    }

    public void setCustomer_dob(String customer_dob) {
        this.customer_dob = customer_dob;
    }

    public boolean isCustomer_gender() {
        return customer_gender;
    }

    public void setCustomer_gender(boolean customer_gender) {
        this.customer_gender = customer_gender;
    }

    public String getCustomer_phonenumber() {
        return customer_phonenumber;
    }

    public void setCustomer_phonenumber(String customer_phonenumber) {
        this.customer_phonenumber = customer_phonenumber;
    }

    public int getAccount_ID() {
        return account_ID;
    }

    public void setAccount_ID(int account_ID) {
        this.account_ID = account_ID;
    }


}
