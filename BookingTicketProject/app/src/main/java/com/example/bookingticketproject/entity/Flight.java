package com.example.bookingticketproject.entity;

import java.util.Date;

public class Flight {
    private String flight_ID;
    private String brand_flight;
    private String address_from;
    private String address_to;
    private String departure_day;
    private String getTime_flight_from;
    private String getTime_flight_to;
    private int economy_total;
    private float economy_price;
    private int business_total;
    private float business_price;
    private int firstclass_total;
    private float firstclass_price;
    private float coupon;
    private String image;

    public Flight() {
    }

    public Flight(String flight_ID, String brand_flight, String address_from, String address_to, String departure_day, String getTime_flight_from, String getTime_flight_to, int economy_total, float economy_price, int business_total, float business_price, int firstclass_total, float firstclass_price, float coupon, String image) {
        this.flight_ID = flight_ID;
        this.brand_flight = brand_flight;
        this.address_from = address_from;
        this.address_to = address_to;
        this.departure_day = departure_day;
        this.getTime_flight_from = getTime_flight_from;
        this.getTime_flight_to = getTime_flight_to;
        this.economy_total = economy_total;
        this.economy_price = economy_price;
        this.business_total = business_total;
        this.business_price = business_price;
        this.firstclass_total = firstclass_total;
        this.firstclass_price = firstclass_price;
        this.coupon = coupon;
        this.image = image;
    }

    public String getFlight_ID() {
        return flight_ID;
    }

    public void setFlight_ID(String flight_ID) {
        this.flight_ID = flight_ID;
    }

    public String getBrand_flight() {
        return brand_flight;
    }

    public void setBrand_flight(String brand_flight) {
        this.brand_flight = brand_flight;
    }

    public String getAddress_from() {
        return address_from;
    }

    public void setAddress_from(String address_from) {
        this.address_from = address_from;
    }

    public String getAddress_to() {
        return address_to;
    }

    public void setAddress_to(String address_to) {
        this.address_to = address_to;
    }

    public String getDeparture_day() {
        return departure_day;
    }

    public void setDeparture_day(String departure_day) {
        this.departure_day = departure_day;
    }

    public String getGetTime_flight_from() {
        return getTime_flight_from;
    }

    public void setGetTime_flight_from(String getTime_flight_from) {
        this.getTime_flight_from = getTime_flight_from;
    }

    public String getTime_flight_from() {
        return getTime_flight_from;
    }

    public void setTime_flight_from(String getTime_flight_from) {
        this.getTime_flight_from = getTime_flight_from;
    }

    public String getGetTime_flight_to() {
        return getTime_flight_to;
    }

    public void setGetTime_flight_to(String getTime_flight_to) {
        this.getTime_flight_to = getTime_flight_to;
    }

    public int getEconomy_total() {
        return economy_total;
    }

    public void setEconomy_total(int economy_total) {
        this.economy_total = economy_total;
    }

    public float getEconomy_price() {
        return economy_price;
    }

    public void setEconomy_price(float economy_price) {
        this.economy_price = economy_price;
    }

    public int getBusiness_total() {
        return business_total;
    }

    public void setBusiness_total(int business_total) {
        this.business_total = business_total;
    }

    public float getBusiness_price() {
        return business_price;
    }

    public void setBusiness_price(float business_price) {
        this.business_price = business_price;
    }

    public int getFirstclass_total() {
        return firstclass_total;
    }

    public void setFirstclass_total(int firstclass_total) {
        this.firstclass_total = firstclass_total;
    }

    public float getFirstclass_price() {
        return firstclass_price;
    }

    public void setFirstclass_price(float firstclass_price) {
        this.firstclass_price = firstclass_price;
    }

    public float getCoupon() {
        return coupon;
    }

    public void setCoupon(float coupon) {
        this.coupon = coupon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
