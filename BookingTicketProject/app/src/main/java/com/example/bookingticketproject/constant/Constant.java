package com.example.bookingticketproject.constant;

public class Constant {

    public static final String DATABASE_NAME = "BookingFlights.db";
    public static final int VERSION = 1;

    // Accounts
    public static final String ACCOUNT_TABLE_NAME = "Accounts";
    public static final String ACCOUNT_COLUMN_ACCOUNT_ID = "account_ID";
    public static final String ACCOUNT_COLUMN_ACCOUNT_EMAIL = "account_email";
    public static final String ACCOUNT_COLUMN_ACCOUNT_PASSWORD = "account_password";
    public static final String ACCOUNT_COLUMN_ACCOUNT_ROLE = "account_role";
    public static final String ACCOUNT_COLUMN_STATUS = "status";

    // Booking Flights
    public static final String BOOKING_FLIGHT_TABLE_NAME = "BookingFlights";
    public static final String BOOKING_FLIGHT_COLUMN_ACCOUNT_ID = "account_ID";
    public static final String BOOKING_FLIGHT_COLUMN_FLIGHT_ID = "flight_ID";
    public static final String BOOKING_FLIGHT_COLUMN_ROUND_TRIP = "round_trip";
    public static final String BOOKING_FLIGHT_COLUMN_PASSENGER = "passenger";
    public static final String BOOKING_FLIGHT_COLUMN_SEAT_CLASS = "seat_class";
    public static final String BOOKING_FLIGHT_COLUMN_BOOKING_DATE = "booking_date";

    // Customers
    public static final String CUSTOMER_TABLE_NAME = "Customers";
    public static final String CUSTOMER_COLUMN_CUSTOMER_ID = "customer_ID";
    public static final String CUSTOMER_COLUMN_CUSTOMER_FULL_NAME = "customer_full_name";
    public static final String CUSTOMER_COLUMN_CUSTOMER_ADDRESS = "customer_address";
    public static final String CUSTOMER_COLUMN_CUSTOMER_DOB = "customer_dob";
    public static final String CUSTOMER_COLUMN_CUSTOMER_GENDER = "customer_gender";
    public static final String CUSTOMER_COLUMN_CUSTOMER_PHONE_NUMBER = "customer_phone_number";
    public static final String CUSTOMER_COLUMN_ACCOUNT_ID = "account_ID";

    // Flights
    public static final String FLIGHT_TABLE_NAME = "Flights";
    public static final String FLIGHT_COLUMN_FLIGHT_ID = "flight_ID";
    public static final String FLIGHT_COLUMN_BRAND_FLIGHT = "brand_flight";
    public static final String FLIGHT_COLUMN_ADDRESS_FROM = "address_from";
    public static final String FLIGHT_COLUMN_ADDRESS_TO = "address_to";
    public static final String FLIGHT_COLUMN_DEPARTURE_DAY = "departure_day";
    public static final String FLIGHT_COLUMN_TIME_FLIGHT_FROM = "time_flight_from";
    public static final String FLIGHT_COLUMN_TIME_FLIGHT_TO = "getTime_flight_to";
    public static final String FLIGHT_COLUMN_ECONOMY_TOTAL = "economy_total";
    public static final String FLIGHT_COLUMN_ECONOMY_PRICE = "economy_price";
    public static final String FLIGHT_COLUMN_BUSINESS_TOTAL = "business_total";
    public static final String FLIGHT_COLUMN_BUSINESS_PRICE = "business_price";
    public static final String FLIGHT_COLUMN_FIRST_CLASS_TOTAL = "first_class_total";
    public static final String FLIGHT_COLUMN_FIRST_CLASS_PRICE = "first_class_price";
    public static final String FLIGHT_COLUMN_COUPON = "coupon";
    public static final String FLIGHT_COLUMN_IMAGE = "image";

    // Create table queries ------------------------------------------------------------------------
    public static String CREATE_TABLE_ACCOUNT = " CREATE TABLE " + ACCOUNT_TABLE_NAME + " ( "
            + ACCOUNT_COLUMN_ACCOUNT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ACCOUNT_COLUMN_ACCOUNT_EMAIL + " TEXT, "
            + ACCOUNT_COLUMN_ACCOUNT_PASSWORD + " TEXT, "
            + ACCOUNT_COLUMN_ACCOUNT_ROLE + " TEXT, "
            + ACCOUNT_COLUMN_STATUS + " boolean) ";

    public static String CREATE_TABLE_BOOKING_FLIGHT = " CREATE TABLE " + BOOKING_FLIGHT_TABLE_NAME + " ( "
            + BOOKING_FLIGHT_COLUMN_ACCOUNT_ID + " INTEGER , "
            + BOOKING_FLIGHT_COLUMN_FLIGHT_ID + " TEXT, "
            + BOOKING_FLIGHT_COLUMN_ROUND_TRIP + " TEXT, "
            + BOOKING_FLIGHT_COLUMN_PASSENGER + " INTEGER, "
            + BOOKING_FLIGHT_COLUMN_SEAT_CLASS + " TEXT, "
            + BOOKING_FLIGHT_COLUMN_BOOKING_DATE + " TEXT, "
            + "FOREIGN KEY (" + BOOKING_FLIGHT_COLUMN_ACCOUNT_ID + ") REFERENCES " + ACCOUNT_TABLE_NAME + "(" + ACCOUNT_COLUMN_ACCOUNT_ID + "), "
            + "FOREIGN KEY (" + BOOKING_FLIGHT_COLUMN_FLIGHT_ID + ") REFERENCES " + FLIGHT_TABLE_NAME + "(" + FLIGHT_COLUMN_FLIGHT_ID + "))";

    public static String CREATE_TABLE_CUSTOMER = " CREATE TABLE " + CUSTOMER_TABLE_NAME + " ( "
            + CUSTOMER_COLUMN_CUSTOMER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CUSTOMER_COLUMN_CUSTOMER_FULL_NAME + " TEXT, "
            + CUSTOMER_COLUMN_CUSTOMER_ADDRESS + " TEXT, "
            + CUSTOMER_COLUMN_CUSTOMER_DOB + " TEXT, "
            + CUSTOMER_COLUMN_CUSTOMER_GENDER + " bit, "
            + CUSTOMER_COLUMN_CUSTOMER_PHONE_NUMBER + " TEXT, "
            + CUSTOMER_COLUMN_ACCOUNT_ID + " INTEGER, "
            + "FOREIGN KEY (" + CUSTOMER_COLUMN_ACCOUNT_ID + ") REFERENCES " + ACCOUNT_TABLE_NAME + "(" + ACCOUNT_COLUMN_ACCOUNT_ID + "))";

    public static String CREATE_TABLE_FLIGHT = " CREATE TABLE " + FLIGHT_TABLE_NAME + " ( "
            + FLIGHT_COLUMN_FLIGHT_ID + " TEXT PRIMARY KEY, "
            + FLIGHT_COLUMN_BRAND_FLIGHT + " TEXT, "
            + FLIGHT_COLUMN_ADDRESS_FROM + " TEXT, "
            + FLIGHT_COLUMN_ADDRESS_TO + " TEXT, "
            + FLIGHT_COLUMN_DEPARTURE_DAY + " TEXT, "
            + FLIGHT_COLUMN_TIME_FLIGHT_FROM + " TEXT, "
            + FLIGHT_COLUMN_TIME_FLIGHT_TO + " TEXT, "
            + FLIGHT_COLUMN_ECONOMY_TOTAL + " INTEGER, "
            + FLIGHT_COLUMN_ECONOMY_PRICE + " float, "
            + FLIGHT_COLUMN_BUSINESS_TOTAL + " INTEGER, "
            + FLIGHT_COLUMN_BUSINESS_PRICE + " float, "
            + FLIGHT_COLUMN_FIRST_CLASS_TOTAL + " INTEGER, "
            + FLIGHT_COLUMN_FIRST_CLASS_PRICE + " float, "
            + FLIGHT_COLUMN_COUPON + " float, "
            + FLIGHT_COLUMN_IMAGE + " TEXT) ";
    // ---------------------------------------------------------------------------------------------

}
